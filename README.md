#Interview Answers#
##General Comment##
As a Java developer, I found Java questions are quite manageable. I spent more than half of the time on MySQL questions. 
I'm no stranger to databases. I'm decent at coming up with optimized query. However, I rarely write any stored procedures
or functions. I'd love to go over the MySQL solutions with a developer if possible. 
##MySQL##
**Please see answers.sql file in the resource directory for answers**
##JAVA##
**Java answers can be found in Java module**

1. `PalindromeChecker.java` contains answer to question 1
2. `Komplementer.java` contains answer to question 2
3. package `phrase` contains answer to question 3. I wrote a Phrase generator class in the test folder for generating large text files.

Appropriate tests can be found in the test packages. I added appropriate comments in the class files.
