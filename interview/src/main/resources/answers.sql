
/* QUESTION 1
Write a query to rank order the following table in MySQL by votes, display the rank as one of the columns.
CREATE TABLE votes ( name CHAR(10), votes INT );
INSERT INTO votes VALUES
('Smith',10), ('Jones',15), ('White',20), ('Black',40), ('Green',50), ('Brown',20);
*/

SELECT name, votes, @currentRank := @currentRank + 1 AS rank
FROM votes v, (SELECT @currentRank := 0) r
ORDER BY votes desc;

/* QUESTION 2
Write a function to capitalize the first letter of a word in a given string;
Example: initcap(UNITED states Of AmERIca) = United States Of America
*/

CREATE FUNCTION initcap (input VARCHAR(255))
RETURNS VARCHAR(255)
DETERMINISTIC
BEGIN
  DECLARE i INT;
  DECLARE len INT;
  SET i = 0;
  SET len = CHAR_LENGTH(input);
  SET input = lower(input);
  WHILE i < len DO
    IF (i = 0 OR MID(input, i - 1, 1) = ' ') THEN
      SET input = CONCAT(LEFT(input, i - 1), UPPER(MID(input, i, 1)), RIGHT(input, len - i - 1));
    END IF;
    SET i = i + 1;
  END WHILE;
  return input;
END;


/* QUESTION 3
Write a procedure in MySQL to split a column into rows using a delimiter.
CREATE TABLE sometbl ( ID INT, NAME VARCHAR(50) );
INSERT INTO sometbl VALUES  (1, 'Smith'),  (2, 'Julio|Jones|Falcons'),
(3, 'White|Snow'),  (4, 'Paint|It|Red'),  (5, 'Green|Lantern'),  (6, 'Brown|bag');
For (2), example rows would look like >> “3, white”, “3, Snow” …
 */
CREATE PROCEDURE `SPLIT_SOMETBL`()
NOT DETERMINISTIC
  BEGIN
    DECLARE r INT DEFAULT 0;
    DECLARE i INT DEFAULT 0;
    DECLARE temp_name VARCHAR(50);

    DECLARE cursor_id INT DEFAULT 0;
    DECLARE cursor_name VARCHAR(50);
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id, name FROM sometbl;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP #loop over every row in the table
      FETCH cursor_i into cursor_id, cursor_name;
      IF done THEN
        LEAVE read_loop;
      END IF;

      SET i = 0;
      SET r = CHAR_LENGTH(cursor_name) - CHAR_LENGTH(REPLACE(cursor_name, '|', '')) + 1; # number of values separated by '|'
      WHILE i < r AND r != 1 DO # loop over every value separated by '|'
        # find the '|' delimited value. first by finding the substring contain the desired value.
        # then remove all character before '|' before the desired value, and then strip the '|' before it.
        SET temp_name = REPLACE(SUBSTRING(SUBSTRING_INDEX(cursor_name, '|', i + 1),
                                          LENGTH(SUBSTRING_INDEX(cursor_name, '|', i)) + 1),
                                '|',
                                '');
        # split name and insert rows
        INSERT INTO sometbl(id, name) VALUES (cursor_id, temp_name);

        # delete row with '|' separated name
        DELETE FROM sometbl WHERE id = cursor_id AND NAME = cursor_name;
        SET i = i + 1;
      END WHILE;

    END LOOP;
    CLOSE cursor_i;
  END;


/* QUESTION 4
I have a table for bugs from a bug tracking software; let’s call the table “bugs”. The table has four columns
(id, open_date, close_date, severity). On any given day a bug is open if the open_date is on or before that day and
close_date is after that day. For example, a bug is open on “2012-01-01”, if it’s created on or before “2012-01-01”
and closed on or after “2012-01-02”. I want a SQL to show number of bugs open for a range of dates.
 */

CREATE TABLE bugs (id INT, open_date DATE, close_date DATE, severity INT);

INSERT INTO bugs VALUES
  (1,'2015-01-01', '2015-01-02', 1),
  (2,'2015-01-02', '2015-01-03', 2),
  (3,'2015-01-03', '2015-01-04', 3);

# a range of dates is from date1 to date2. date1 should be before date2

SELECT COUNT(id)
FROM bugs
WHERE open_date <= '2015-01-02' #date2
      AND close_date > '2015-01-02' #date1
      AND close_date >= DATE_ADD(open_date, INTERVAL 1 DAY);
