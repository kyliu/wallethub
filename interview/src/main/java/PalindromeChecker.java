/**
 * Created by quincy on 15-07-25.
 */
public class PalindromeChecker {
    /**
     *
     * Check if given input is a palindrome
     *
     * @param input input string to check
     * @return true if input is a palindrome. false otherwise. An empty string is not palindrome
     */
    public boolean check(String input) {

        if (input == null || input.length() == 0) {
            return false;
        }

        /**
         * adding char value of first half of the string, then subtract second half.
         */
        char[] inputChar = input.toCharArray();
        long sum = 0;
        for (int i = 0; i < inputChar.length; i++) {
            if (i < inputChar.length / 2) {
                sum += (int) inputChar[i];
            } else if (inputChar.length % 2 != 0 && i == inputChar.length / 2) {
                //do nothing when i is the middle element in an odd length of a string
                sum += 0;
            } else {
                sum -= (int) inputChar[i];
            }

        }
        return sum == 0;
    }
}
