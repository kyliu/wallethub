package phrase;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlBatch;
import org.skife.jdbi.v2.sqlobject.SqlQuery;

import java.util.List;

/**
 * DAO for inserting and retrieving phrase records
 *
 * Created by quincy on 15-07-29.
 */
public interface PhraseDao {
    // use batch insert to speed things up when importing from file to database
    @SqlBatch("INSERT INTO PHRASE (PHRASE) VALUES (:phrases)")
    void insertPhrase(@Bind("phrases") List<String> phrases);

    // query for most frequent phrase in descending order
    @SqlQuery("SELECT phrase FROM phrase GROUP BY phrase ORDER BY COUNT(phrase) DESC LIMIT :mostFrequentLimit")
    List<String> getMostFrequentPhrases(@Bind("mostFrequentLimit") int mostFrequentLimit);
}
