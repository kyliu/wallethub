package phrase;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 * The idea for this solution is to import text file into a MySQL database, then query for the most frequent phrases.
 *
 * I used Stream in java NIO to stream the text file to limit memory footprint, and forEach function is multi-threaded
 * to speed up importing.
 *
 * During importing I opted to use batched import to ease database load.
 *
 * When writing result to a text file, I used buffered writer to limit memory usage and speed up file writing.
 *
 * A 11.9 GB text file takes about 26.26 minutes to process on a machine with 16 GB RAM and SSD
 *
 * Created by quincy on 15-07-29.
 */
public class Phrase {

    public static void main(String[] args) {
        String databaseName = "./phraseDB";
        String filename = "./interview/phrase.txt";
        char delimiter = '|';
        int mostFrequentLimit = 5;

        // delete previous database file if it exists
        File databaseFile = new File(databaseName + ".mv.db");
        if (databaseFile.exists()) {
            databaseFile.delete();
            System.out.println("Deleted previous database file.");
        }

        DBI database = new DBI("jdbc:h2:file:" + databaseName + ";MODE=MySQL"); // enable MySQL mode

        try (Handle handle = database.open()) {
            handle.execute("CREATE TABLE PHRASE (phrase TEXT)");
            handle.commit();

            Date begin = new Date();
            System.out.println("Start processing " + filename + " at " + begin.toString());

            Path filePath = Paths.get(filename);

            PhraseDao phraseDao = handle.attach(PhraseDao.class);

            try (Stream<String> lines = Files.lines(filePath)) {
                /*
                import phrases one line at a time. Note forEach does not guarantee order.
                 */
                lines.forEach((line) -> {
                    String[] phrases = line.split("\\" + delimiter);
                    phraseDao.insertPhrase(Arrays.asList(phrases));
                });
            } catch (IOException e) {
                System.err.println("Fail to read file " + filename);
                e.printStackTrace();
                return;
            }
            handle.commit();
            Date end = new Date();
            System.out.println("Finished processing file " + filename + " at End time: " + end.toString() + ". Took " +
                    (end.getTime() - begin.getTime()) / 1000.0 / 60.0 + " minutes.");

            List<String> result = phraseDao.getMostFrequentPhrases(mostFrequentLimit);
            String resultFilename = filename + "_result.txt";
            try (FileWriter fileWriter  = new FileWriter(resultFilename)) {
                try (BufferedWriter writer = new BufferedWriter(fileWriter)) {
                    result.forEach((phrase) -> {
                        try {
                            writer.write(phrase);
                            writer.newLine();
                        } catch (IOException e) {
                            System.err.println("Fail to write file " + resultFilename);
                            e.printStackTrace();
                            return;
                        }
                    });
                    writer.flush();
                }
            } catch (IOException e) {
                System.err.println("Fail to write file " + resultFilename);
                e.printStackTrace();
                return;
            }
            System.out.println("Result can be found in file " + resultFilename + " since " + new Date().toString());
        }
    }
}
