import java.util.*;

/**
 * Created by quincy on 15-07-25.
 */
public class Komplementer {

    /**
     * Find all k-complement indices in the given array
     *
     * @param A array contains integers
     * @param k the sum two complement number must add up to
     * @return a list of indices
     */
    public List<int[]> komplement(int[] A, int k) {
        Map<Integer, List<Integer>> numbers = new HashMap<Integer, List<Integer>>();
        List<int[]> result = new LinkedList<>();

        /*
        go through array and put number and its indices (when there is more than one occurance of a number).
         */
        for(int i = 0; i < A.length; i++) {
            int currentNumber = A[i];
            if (numbers.containsKey(currentNumber)) {
                List<Integer> indices = numbers.get(currentNumber);
                indices.add(i);
                numbers.put(currentNumber, indices);
            } else {
                List<Integer> indices = new LinkedList<>();
                indices.add(i);
                numbers.put(currentNumber, indices);
            }
        }

        // avoid duplicates
        Set<Integer> usedIndices = new HashSet<Integer>();

        for (int i = 0; i < A.length; i++) {
            if (!usedIndices.contains(i)) {
                int komplement = k - A[i];
                if (numbers.containsKey(komplement)) {
                    usedIndices.add(i);
                    for(Integer index : numbers.get(komplement)) {
                        result.add(new int[]{i, index});
                        usedIndices.add(index);
                    }
                }
            }
        }

        return result;
    }
}
