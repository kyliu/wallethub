import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by quincy on 15-07-25.
 */
public class TestPalindromeChecker {
    PalindromeChecker palindromeChecker;

    @Before
    public void setUp() {
        palindromeChecker = new PalindromeChecker();
    }

    @Test
    public void testCheckTrue() {
        String input = "1221";
        assertTrue(input + " is a palindrome.", palindromeChecker.check(input));

        input = "12321";
        assertTrue(input + " is a palindrome.", palindromeChecker.check(input));
    }

    @Test
    public void testCheckFail() {
        String input = "asdfdasoihwery98w7432";
        assertFalse(input + " is not a palindrome.", palindromeChecker.check(input));

        input = "2jdnaoidhf";
        assertFalse(input + " is not a palindrome.", palindromeChecker.check(input));
    }

    @Test
    public void testCheckEmpty() {
        assertFalse("Empty string is not a palindrome", palindromeChecker.check(""));
    }
}
