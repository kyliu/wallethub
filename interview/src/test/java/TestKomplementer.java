import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by quincy on 15-07-26.
 */
public class TestKomplementer {
    Komplementer komplementer;

    @Before
    public void setUp() {
        komplementer = new Komplementer();
    }

    @Test
    public void testKomplementerPass() {
        int[] numbers = {1, 2, 3, 4, 17, 18, 19, 20};
        int k = 21;

        List<int[]> expected = Arrays.asList(new int[]{0, 7},
                new int[]{1, 6},
                new int[]{2, 5},
                new int[]{3, 4});

        List<int[]> result = komplementer.komplement(numbers, k);


        expected.forEach(pair -> System.out.println(Arrays.toString(pair)));
        result.forEach(pair -> System.out.println(Arrays.toString(pair)));

        assertArrayEquals("Result is " + Arrays.toString(result.toArray()), expected.toArray(), result.toArray());
    }

    @Test
    public void testKomplementerFail() {
        int[] numbers = {1, 2, 3, 4, 17, 18, 19, 20};
        int k = 27;

        List<int[]> result = komplementer.komplement(numbers, k);

        assertTrue("Result is empty.", result.isEmpty());

        result = komplementer.komplement(new int[]{}, k);
        assertTrue("Empty input produce empty result.", result.isEmpty());
    }

    @Test
    public void testKomplementNegative() {
        int[] numbers = {-1, -2, -3, -4, 17, 18, 19, 20};
        int k = 16;

        List<int[]> expected = Arrays.asList(new int[]{0, 4},
                new int[]{1, 5},
                new int[]{2, 6},
                new int[]{3, 7});

        List<int[]> result = komplementer.komplement(numbers, k);

        assertArrayEquals("Result is " + Arrays.toString(result.toArray()), expected.toArray(), result.toArray());
    }

    @Test
    public void testKomplementDuplicates() {
        int[] numbers = {-1, -2, -3, -4, -5, -5, -5, 17, 18, 19, 20, 21, 21, 21};
        int k = 16;

        List<int[]> expected = Arrays.asList(new int[]{0, 7},
                new int[]{1, 8},
                new int[]{2, 9},
                new int[]{3, 10},
                new int[]{4, 11},
                new int[]{4, 12},
                new int[]{4, 13},
                new int[]{5, 11},
                new int[]{5, 12},
                new int[]{5, 13},
                new int[]{6, 11},
                new int[]{6, 12},
                new int[]{6, 13});

        List<int[]> result = komplementer.komplement(numbers, k);

        result.forEach(pair -> System.out.println(Arrays.toString(pair)));

        assertArrayEquals("Result is " + Arrays.toString(result.toArray()), expected.toArray(), result.toArray());
    }

    @Test
    public void testKomplementerPassUnsorted() {
        int[] numbers = {19, 17, 2, 3, 20, 1, 4, 18};
        int k = 21;

        List<int[]> expected = Arrays.asList(new int[]{0, 2},
                new int[]{1, 6},
                new int[]{3, 7},
                new int[]{4, 5});

        List<int[]> result = komplementer.komplement(numbers, k);

        expected.forEach(pair -> System.out.println(Arrays.toString(pair)));
        result.forEach(pair -> System.out.println(Arrays.toString(pair)));

        assertArrayEquals("Result is " + Arrays.toString(result.toArray()), expected.toArray(), result.toArray());
    }
}
