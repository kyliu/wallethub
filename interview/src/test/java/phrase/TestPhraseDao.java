package phrase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by quincy on 15-07-29.
 */
public class TestPhraseDao extends TestDatabase {
    PhraseDao phraseDao;

    @Before
    public void setUp(){
        super.setUp();
        phraseDao = handle.attach(PhraseDao.class);
    }

    @After
    public void tearDown() {
        super.tearDown();
    }

    @Test
    public void testInsertPhrase() {
        phraseDao.insertPhrase(Arrays.asList("hello world", "apple", "banana"));

        Object result = handle.createQuery("SELECT COUNT(phrase) FROM PHRASE").first().get("COUNT(phrase)");

        assertEquals("New inserted phrase should have count of 2", Integer.valueOf(3), Integer.valueOf(result.toString()));
    }

    @Test
    public void testGetMostFrequentPhrases() {
        handle.createStatement("INSERT INTO PHRASE VALUES ('123'), ('123'), ('123'), ('123')," +
                " ('1'), ('1'), ('1'), ('1'), ('1'), " +
                "('number 1'), " +
                "('number 1'), " +
                "('number 2'), " +
                "('number 3') ").execute();

        List<String> phrases = phraseDao.getMostFrequentPhrases(3);

        phrases.forEach((phrase)->System.out.println(phrase));
        assertEquals("There should only be specified number of phrases here.", 3, phrases.size());
        assertTrue("Verify list content.", phrases.containsAll(Arrays.asList("123", "1", "number 1")));
    }
}
