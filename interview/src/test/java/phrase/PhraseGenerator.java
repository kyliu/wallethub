package phrase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Generates phrase.txt file containing pre-defined phrases with pre-defined frequencies.
 *
 * Created by quincy on 15-07-27.
 */
public class PhraseGenerator {
    String filename;
    int phrasesPerLine;
    String[] phrases;
    int numberOfLines = 10000000;

    @Before
    public void setUp() {
        filename = "phrase.txt";
        phrasesPerLine = 50;
        phrases = new String[]{"most frequent phrase",
                "second most frequent phrase",
                "third most frequent phrase",
                "fourth most frequent phrase",
                "fifth most frequent phrase",
                "sixth most frequent phrase",
                "seventh most frequent phrase",
                "eighth most frequent phrase",
                "ninth most frequent phrase",
                "tenth most frequent phrase"
        };
    }

    @After
    public void tearDown() {

    }

    public String getString() {
        double ran = Math.random();
        int index;
        if (ran > 1.0/2) {
            index = 0;
        } else if (ran > 1.0/2/2) {
            index = 1;
        } else if (ran > 1.0/2/2/2) {
            index = 2;
        }else if (ran > 1.0/2/2/2/2) {
            index = 3;
        }else if (ran > 1.0/2/2/2/2/2) {
            index = 4;
        }else if (ran > 1.0/2/2/2/2/2/2) {
            index = 5;
        }else if (ran > 1.0/2/2/2/2/2/2/2) {
            index = 6;
        }else if (ran > 1.0/2/2/2/2/2/2/2/2) {
            index = 7;
        }else if (ran > 1.0/2/2/2/2/2/2/2/2/2) {
            index = 8;
        }else {
            index = 9;
        }
        return phrases[index];
    }

    @Test
    public void generate() throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
            for (int i = 0; i < numberOfLines; i++) {
                for (int j = 0; j < phrasesPerLine; j++) {
                    writer.write(getString()+'|');
                }
                writer.newLine();
            }
            writer.flush();
        }
    }
}
