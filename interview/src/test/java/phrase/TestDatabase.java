package phrase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

/**
 * Created by quincy on 15-07-29.
 */
public class TestDatabase {
    DBI database;
    Handle handle;

    @Before
    public void setUp() {
        database = new DBI("jdbc:h2:mem:"+ UUID.randomUUID()+";MODE=MySQL");
        handle = database.open();

        handle.execute("CREATE TABLE PHRASE (phrase TEXT)");
    }

    @After
    public void tearDown() {
        handle.execute("DROP TABLE PHRASE");
        handle.close();
    }

    @Test
    public void testDatabase() {
        handle.insert("INSERT INTO PHRASE VALUES ('Mike')");

        Map<String, Object> obj= handle.select("SELECT phrase FROM PHRASE WHERE phrase = 'Mike'").get(0);

        String phrase = obj.get("phrase").toString();

        System.out.println(phrase);
        assertEquals("Inserted phrase is mike", "clob0: 'Mike'", phrase);
    }
}
